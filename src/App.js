import './App.css';
import React from "react"



class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      key: "",
      template: ""
    };
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleId = this.handleId.bind(this);
  }
  componentDidMount() {
    document.addEventListener("keydown", this.handleKeyPress);
    document.addEventListener("click", this.handleId)
  };

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeyPress);
    document.addEventListener("click", this.handleId)
  }


  handleKeyPress(event) {
    if (bankOneOn) {
      let arg = bankOne.find((element) => {
        return element.keyCode === event.keyCode;
      });
      this.setState({
        template: arg.id
      })
      new Audio(arg.url).play()
    } else {
      let arg = bankTwo.find((element) => {
        return element.keyCode === event.keyCode;
      });
      this.setState({
        template: arg.id
      })
      new Audio(arg.url).play()

    }


  };

  handleId(event) {
    if (event.target.value === undefined) {
      this.setState((state) => ({
        template: state.template
      }))
    } else {
      this.setState({
        template: event.target.value
      })
    }
  }


  render() {
    return (
      <div id="root">
        <div className='inner-container'>
          <Buttons />
          <Panel template={this.state.template} />
        </div>
      </div>
    );
  }
}

export default App;


function Buttons() {



  const buttonsPadOne = bankOne.map(btn => {
    console.log(btn.id)
    return <button key={btn.keyCode} value={btn.id} id={btn.keyTrigger} onClick={() => {
      new Audio(btn.url).play();
    }
  } >{btn.keyTrigger}</button>

      
  });

  const buttonsPadTwo = bankTwo.map(btn => {
    return <button key={btn.keyCode} value={btn.id} id={btn.keyTrigger} onClick={() => {
      new Audio(btn.url).play();
    }
  } >{btn.keyTrigger}</button>;
  });


  return (
    buttonPressedPower ? 
    buttonPressed ?
      <div id='buttons'>
        {buttonsPadOne}
      </div>
      :
      <div id='buttons'>
        {buttonsPadTwo}
      </div>
      :
      <div>
        <h3>Turn on the Drum Machine</h3>
      </div>
  )
}


class Panel extends React.Component {

  constructor(props) {
    super(props);
    this.buttonBank = this.buttonBank.bind(this);
    this.buttonPower = this.buttonPower.bind(this)
  }

  buttonBank() {
    buttonPressed = !buttonPressed
    bankOneOn = !bankOneOn
  }

  buttonPower() {
    buttonPressedPower = !buttonPressedPower
    
  }

  render() {


    return (
      buttonPressedPower ? 
      <div id="panel">
        <div><h3>Power</h3>
          <button id='switch' type='button'
            className={buttonPressedPower ? "buttonOn" : "buttonOff"}
            onClick={this.buttonPower}
          ></button>
        </div>
        <div ><p id='template'>{this.props.template}</p></div>
        <div><input type="range"></input></div>
        <div><h3>Bank</h3>
          <button id='switch' type='button'
            className={buttonPressed ? "buttonOn" : "buttonOff"}
            onClick={this.buttonBank}
          ></button>
        </div>
      </div>
      :
      <div id="panel">
        <div><h3>Power</h3>
          <button id='switch' type='button'
            className={buttonPressedPower ? "buttonOn" : "buttonOff"}
            onClick={this.buttonPower}
          ></button>
        </div>
        <div ><p id='template'></p></div>
        <div><input type="range" disabled></input></div>
        <div><h3>Bank</h3>
          <button id='switch' type='button'
          ></button>
        </div>
      </div>
    )
  }


}

let buttonPressed = true;
let buttonPressedPower = true;
let bankOneOn = true;

const bankOne = [
  {
    keyCode: 81,
    keyTrigger: 'Q',
    id: 'Heater-1',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-1.mp3'
  },
  {
    keyCode: 87,
    keyTrigger: 'W',
    id: 'Heater-2',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-2.mp3'
  },
  {
    keyCode: 69,
    keyTrigger: 'E',
    id: 'Heater-3',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-3.mp3'
  },
  {
    keyCode: 65,
    keyTrigger: 'A',
    id: 'Heater-4',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-4_1.mp3'
  },
  {
    keyCode: 83,
    keyTrigger: 'S',
    id: 'Clap',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-6.mp3'
  },
  {
    keyCode: 68,
    keyTrigger: 'D',
    id: 'Open-HH',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Dsc_Oh.mp3'
  },
  {
    keyCode: 90,
    keyTrigger: 'Z',
    id: "Kick-n'-Hat",
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Kick_n_Hat.mp3'
  },
  {
    keyCode: 88,
    keyTrigger: 'X',
    id: 'Kick',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/RP4_KICK_1.mp3'
  },
  {
    keyCode: 67,
    keyTrigger: 'C',
    id: 'Closed-HH',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Cev_H2.mp3'
  }
];

const bankTwo = [
  {
    keyCode: 81,
    keyTrigger: 'Q',
    id: 'Chord-1',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Chord_1.mp3'
  },
  {
    keyCode: 87,
    keyTrigger: 'W',
    id: 'Chord-2',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Chord_2.mp3'
  },
  {
    keyCode: 69,
    keyTrigger: 'E',
    id: 'Chord-3',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Chord_3.mp3'
  },
  {
    keyCode: 65,
    keyTrigger: 'A',
    id: 'Shaker',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Give_us_a_light.mp3'
  },
  {
    keyCode: 83,
    keyTrigger: 'S',
    id: 'Open-HH',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Dry_Ohh.mp3'
  },
  {
    keyCode: 68,
    keyTrigger: 'D',
    id: 'Closed-HH',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Bld_H1.mp3'
  },
  {
    keyCode: 90,
    keyTrigger: 'Z',
    id: 'Punchy-Kick',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/punchy_kick_1.mp3'
  },
  {
    keyCode: 88,
    keyTrigger: 'X',
    id: 'Side-Stick',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/side_stick_1.mp3'
  },
  {
    keyCode: 67,
    keyTrigger: 'C',
    id: 'Snare',
    url: 'https://s3.amazonaws.com/freecodecamp/drums/Brk_Snr.mp3'
  }
];
